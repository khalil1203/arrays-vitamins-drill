const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/




// Question 1:

const answer1 = items.filter((entries) => {
    return entries.available;
});

console.log(answer1);



// Question 2:

const answer2 = items.filter((entries) => {

    return entries.contains == 'Vitamin C';
});

console.log(answer2);



// Question 3:

const answer3 = items.filter((entries) => {

    return entries.contains.includes('Vitamin A');
});

console.log(answer3);


// Question 4:

const answer4 = items.reduce((acc, entries) => {

    const tempstr = entries.contains.split(", ");

    tempstr.map((data) => {

        if (acc[data]) {
            acc[data].push(entries.name);
        }
        else {
            acc[data] = [entries.name];
        }

    });
    return acc;
}, {});

console.log(answer4);



// Question 5 :


const answer5 = items.sort((prev,cur)=>{
    let tempstr1 = prev.contains.split(", ");
    let tempstr2 = cur.contains.split(", ");

    if(tempstr1.length > tempstr2.length){
        return -1;
    }
    else if(tempstr1.length < tempstr2.length){
        return 1;
    }
    return 0;
    

});

console.log(answer5);






